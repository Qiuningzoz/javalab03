//Qiuning Liang
//2037000
package LinearAlgebra;
public class Vector3d{
    private double x;
    private double y;
    private double z;


  
    public Vector3d(double x,double y,double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
  
    public double getX(){
          return this.x;
      }
      public double getY(){
          return this.y;
      }
      public double getZ(){
          return this.z;
      }
  
      public double magnitude(){
          return Math.sqrt(Math.pow(this.x,2) + Math.pow(this.y,2) + Math.pow(this.z,2));
      }
  
      public double dotProduct(Vector3d a){
          return (this.x * a.x) + (this.y * a.y) + (this.z * a.z);
      }
  
      public Vector3d add(Vector3d b){
         Vector3d c = new Vector3d(0,0,0);
         c.x = this.x + b.x;
         c.y = this.y + b.y;
         c.z = this.z + b.z;
          return c;
      }

      
  
  
  }