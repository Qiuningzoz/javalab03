//Qiuning Liang
//2037000
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {

    @Test
    public void testcreates() {
        Vector3d c = new Vector3d(1,2,3);
        assertEquals(c.getX(),1);
        assertEquals(c.getY(),2);
        assertEquals(c.getZ(),3);
    }

     @Test
    public void testmagnitude(){
        Vector3d c = new Vector3d(2,1,4);
        assertEquals(c.magnitude(),4.58257569495584,0.00000000000001);
    }

     @Test
    public void testdotProduct(){
        Vector3d c = new Vector3d(1,2,3);
        Vector3d a = new Vector3d(2,3,4);
        assertEquals(c.dotProduct(a),20);
    }

     @Test
     public void testAdd(){
        Vector3d c = new Vector3d(1,2,3);
        Vector3d v = new Vector3d(2,3,4);
        Vector3d result = c.add(v);
        assertEquals(result.getX(),3);
        assertEquals(result.getY(),5);
        assertEquals(result.getZ(),7);
     }

}
